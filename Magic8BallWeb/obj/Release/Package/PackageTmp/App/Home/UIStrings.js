﻿//Location Specific Strings
var UIStrings = (function () {
    "use strict";
    var UIStrings = {};
    UIStrings.EN =
        {
            "greeting": "Welcome to the Magic 8 Ball",
            "BallAffirm0": "It is certain",
            "BallAffirm1": "It is decidedly so",
            "BallAffirm2": "Without a doubt",
            "BallAffirm3": "Yes definitely",
            "BallAffirm4": "You may rely on it",
            "BallAffirm5": "As I see it yes",
            "BallAffirm6": "Most likely",
            "BallAffirm7": "Outlook good",
            "BallAffirm8": "Yes",
            "BallAffirm9": "Signs point to yes",
            "BallNonCommit0": "Reply hazy try again",
            "BallNonCommit1": "Ask again later",
            "BallNonCommit2": "Better not tell you now",
            "BallNonCommit3": "Cannot predict now",
            "BallNonCommit4": "Concentrate and ask again",
            "BallNeg0": "Don't count on it",
            "BallNeg1": "My reply is no",
            "BallNeg2": "My sources say no",
            "BallNeg3": "Outlook not so good",
            "BallNeg4": "Very doubtful",
            "about": "Ask your question and Shake the ball",
        };

    UIStrings.PL =
        {
            "greeting": "Witaj w magicznej ósemce!",
            "BallAffirm0": "To pewne",
            "BallAffirm1": "Zdecydowanie tak",
            "BallAffirm2": "Bez wątpienia",
            "BallAffirm3": "Pewnie",
            "BallAffirm4": "Możesz na tym polegać",
            "BallAffirm5": "Z mojego punktu widzenia tak",
            "BallAffirm6": "Raczej tak",
            "BallAffirm7": "Wygląda dobrze",
            "BallAffirm8": "Tak",
            "BallAffirm9": "Wszystkie znaki wskazują na tak",
            "BallNonCommit0": "Odpowiedź jest zamglona, spróbuj jeszcze raz",
            "BallNonCommit1": "Zapytaj mnie później",
            "BallNonCommit2": "Chyba lepiej nie mówić o tym teraz",
            "BallNonCommit3": "Nie umiem teraz tego przewidzieć",
            "BallNonCommit4": "Skoncentruj się I zapytaj jeszcze raz",
            "BallNeg0": "Nie licz na to",
            "BallNeg1": "Moja odpowiedź to nie",
            "BallNeg2": "Moje źródła donoszą, że nie",
            "BallNeg3": "Nie wygląda to dobrze",
            "BallNeg4": "Bardzo wątpię",
            "about": "Zadaj pytanie i potrząśnij ósemką",
        };

    UIStrings.FR =
        {
            "greeting": "Bienvenue aux Magic 8 Ball",
            "BallAffirm0": "C’est certain",
            "BallAffirm1": "Très probable",
            "BallAffirm2": "Sans doute",
            "BallAffirm3": "Définitivement",
            "BallAffirm4": "vous pouvez y compter",
            "BallAffirm5": "Je pense que oui",
            "BallAffirm6": "Probablement",
            "BallAffirm7": "Absolument!",
            "BallAffirm8": "Oui",
            "BallAffirm9": "J’en suis certaine",
            "BallNonCommit0": "Je sais pas",
            "BallNonCommit1": "Demandez moi plus tard",
            "BallNonCommit2": "Une chance sur deux",
            "BallNonCommit3": "Impossible de savoir",
            "BallNonCommit4": "Concentrez-vous et redemandez moi",
            "BallNeg0": "N’y comptez pas",
            "BallNeg1": "Je pense que non",
            "BallNeg2": "J’ai entendu que non",
            "BallNeg3": "C’est improbable!",
            "BallNeg4": "Très improbable",
            "about": "Demandez votre question et puis secouer la balle",
        };

    UIStrings.PT =
        {
            "greeting": "Bem-vindo ao Magic 8 Ball",
            "BallAffirm0": "De certeza",
            "BallAffirm1": "Decididamente sim",
            "BallAffirm2": "Sem dúvida",
            "BallAffirm3": "Sim definitivamente",
            "BallAffirm4": "Podes contar com isso",
            "BallAffirm5": "Como eu vejo sim",
            "BallAffirm6": "Mais provável",
            "BallAffirm7": "O resultado parece ser bom",
            "BallAffirm8": "Sim",
            "BallAffirm9": "O sinal aponta para sim",
            "BallNonCommit0": "Resposta estranha tenta novamente",
            "BallNonCommit1": "Pergunta mais tarde",
            "BallNonCommit2": "O melhor é não dizer-te nada por agora",
            "BallNonCommit3": "Não é previsível agora",
            "BallNonCommit4": "Concentra-te e pergunta outra vez",
            "BallNeg0": "Não contes com isso",
            "BallNeg1": "A minha resposta é negativa",
            "BallNeg2": "As minhas fontes dizem que não",
            "BallNeg3": "O resultado não parece bom",
            "BallNeg4": "Muito duvidoso",
            "about": "Faz a tua pergunta e mexe a bola",
        };


    UIStrings.getLocaleStrings = function (locale) {
        var text;

        switch (locale) {
            case 'en-UK':
                text = UIStrings.EN;
                break;
            case 'en-US':
                text = UIStrings.EN;
                break;
            case 'pl':
                text = UIStrings.PL;
                break;
            case 'pt':
                text = UIStrings.PT;
                break;
            case 'pt-br':
                text = UIStrings.PT;
                break;
            case 'fr':
                text = UIStrings.FR;
                break;
            case 'fr-be':
                text = UIStrings.FR;
                break;
            default:
                text = UIStrings.EN;
        }
        return text;
    };
    return UIStrings;
})();