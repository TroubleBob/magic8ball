﻿/// <reference path="../App.js" />

(function () {
    "use strict";

    // The initialize function must be run each time a new page is loaded
    Office.initialize = function (reason) {
        $(document).ready(function () {
            app.initialize();
            //var myLang = Office.context.displayLanguage;
            var myLang = 'en-UK';
            var UIText;

            window.UIText = UIStrings.getLocaleStrings(myLang);

            $("#greeting").text(window.UIText.greeting);
            $("#shakeBall").text(window.UIText.about);
            $("#shakeBall").click(function () {
                
                animate("#ball", 'shake');
                setTimeout(function () { generateAnswer() }, 850);
                
            });
        });
    };
    function animate(element_ID,animation){
        $(element_ID).addClass(animation);
        var wait=window.setTimeout( function() {
            $(element_ID).removeClass(animation)
        },
        2400);
    }

    function generateAnswer() {
        var randomnumber = Math.floor(Math.random() * 20)

        switch (randomnumber) {
            case 0:
                $("#returnedAnswer").text(window.UIText.BallAffirm0);
                break;
            case 1:
                $("#returnedAnswer").text(window.UIText.BallAffirm1);
                break;
            case 2:
                $("#returnedAnswer").text(window.UIText.BallAffirm2);
                break;
            case 3:
                $("#returnedAnswer").text(window.UIText.BallAffirm3);
                break;
            case 4:
                $("#returnedAnswer").text(window.UIText.BallAffirm4);
                break;
            case 5:
                $("#returnedAnswer").text(window.UIText.BallAffirm5);
                break;
            case 6:
                $("#returnedAnswer").text(window.UIText.BallAffirm6);
                break;
            case 7:
                $("#returnedAnswer").text(window.UIText.BallAffirm7);
                break;
            case 8:
                $("#returnedAnswer").text(window.UIText.BallAffirm8);
                break;
            case 9:
                $("#returnedAnswer").text(window.UIText.BallAffirm9);
                break;
            case 10:
                $("#returnedAnswer").text(window.UIText.BallNonCommit0);
                break;
            case 11:
                $("#returnedAnswer").text(window.UIText.BallNonCommit1);
                break;
            case 12:
                $("#returnedAnswer").text(window.UIText.BallNonCommit2);
                break;
            case 13:
                $("#returnedAnswer").text(window.UIText.BallNonCommit3);
                break;
            case 14:
                $("#returnedAnswer").text(window.UIText.BallNonCommit4);
                break;
            case 15:
                $("#returnedAnswer").text(window.UIText.BallNeg0);
                break;
            case 16:
                $("#returnedAnswer").text(window.UIText.BallNeg1);
                break;
            case 17:
                $("#returnedAnswer").text(window.UIText.BallNeg2);
                break;
            case 18:
                $("#returnedAnswer").text(window.UIText.BallNeg3);
                break;
            case 19:
                $("#returnedAnswer").text(window.UIText.BallNeg4);
                break;
        }

    }
})();